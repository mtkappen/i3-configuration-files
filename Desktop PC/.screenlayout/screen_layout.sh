#!/bin/sh
xrandr --output DP-3 --off --output DVI-I-0 --off --output HDMI-0 --mode 1920x1080 --pos 1920x120 --rotate normal --output DP-5 --off --output DP-4 --mode 1280x1024 --pos 3840x176 --rotate normal --output DVI-I-1 --mode 1920x1200 --pos 0x0 --rotate normal --output DP-2 --off --output DP-1 --off --output DP-0 --off
